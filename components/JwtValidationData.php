<?php


namespace app\components;

use Yii;

class JwtValidationData extends \sizeg\jwt\JwtValidationData
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->validationData->setIssuer(Yii::$app->params['baseURL']);
        $this->validationData->setAudience(Yii::$app->params['baseURL']);
        $this->validationData->setId(Yii::$app->params['baseURL']);

        parent::init();
    }
}