<?php


namespace app\controllers;

use app\services\IAuthService;

use Yii;
use yii\rest\Controller;
use sizeg\jwt\JwtHttpBearerAuth;

class AuthController extends Controller
{
    private $authService;

    public function __construct($id, $module, IAuthService $authService, $config = [])
    {
        $this->authService = $authService;
        parent::__construct($id, $module, $config);

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
            ],
        ];

        return $behaviors;
    }

    public function actionLogin()
    {
        $response = Yii::$app->response;
        $token = $this->authService->getToken(Yii::$app->request->post());
        if (empty($token)) {
            $response->statusCode = 422;
            $response->data = [
                'message' => 'Invalid username or password!',
                'token' => $token
            ];
            return $response;
        }
        $response->statusCode = 200;
        $response->data = [
            'message' => 'Login successfully!',
            'token' => $token
        ];
        return $response;
    }

}