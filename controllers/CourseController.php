<?php


namespace app\controllers;

use app\models\resources\Course;

class CourseController extends CustomActiveController
{
    public $modelClass = Course::class;
}