<?php


namespace app\controllers;

use Yii;
use app\models\resources\User;
use app\services\IUserService;

class UserController extends CustomActiveController
{
    public $modelClass = User::class;
    private $service;

    public function __construct($id, $module, IUserService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }
    public function actionCreate()
    {
        //TODO: extract to service
        $data = Yii::$app->request->post();
        return $this->service->createUser($data);
    }
}