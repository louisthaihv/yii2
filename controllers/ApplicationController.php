<?php


namespace app\controllers;

use app\models\resources\Application;
use yii\data\ActiveDataProvider;


class ApplicationController extends CustomActiveController
{
    public $modelClass = Application::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->modelClass::find()->andWhere(['user_id' => \Yii::$app->request->get('userId')])
        ]);
    }
}