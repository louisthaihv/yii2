<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_university_degrees}}`.
 */
class m200502_024325_create_application_university_degrees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_university_degrees}}', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer(),
            'university_degree_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'FK_applications_a_u_d_application_id',
            '{{%application_university_degrees}}',
            'application_id',
            '{{%applications}}',
            'id'
        );
        $this->addForeignKey(
            'FK_university_degrees_a_u_d_university_degree_id',
            '{{%application_university_degrees}}',
            'university_degree_id',
            '{{%university_degrees}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_applications_a_u_d_application_id','{{%application_university_degrees}}');
        $this->dropForeignKey('FK_university_degrees_a_u_d_university_degree_id','{{%application_university_degrees}}');
        $this->dropTable('{{%application_university_degrees}}');
    }
}
