<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%university_degree_courses}}`.
 */
class m200502_025440_create_university_degree_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%university_degree_courses}}', [
            'id' => $this->primaryKey(),
            'university_degree_id' => $this->integer(),
            'course_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'FK_university_degrees_u_d_courses_university_id',
            '{{%university_degree_courses}}',
            'university_degree_id',
            '{{%university_degrees}}',
            'id'
        );
        $this->addForeignKey(
            'FK_courses_university_degree_courses_course_id',
            '{{%university_degree_courses}}',
            'course_id',
            '{{%courses}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_university_degrees_u_d_courses_university_id','{{%university_degree_courses}}');
        $this->dropForeignKey('FK_courses_university_degree_courses_course_id','{{%university_degree_courses}}');
        $this->dropTable('{{%university_degree_courses}}');
    }
}
