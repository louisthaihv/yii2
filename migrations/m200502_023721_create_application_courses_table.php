<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_courses}}`.
 */
class m200502_023721_create_application_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_courses}}', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer(),
            'course_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'FK_applications_application_course_application_id',
            '{{%application_courses}}',
            'application_id',
            '{{%applications}}',
            'id'
        );
        $this->addForeignKey(
            'FK_courses_application_course_course_id',
            '{{%application_courses}}',
            'course_id',
            '{{%courses}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_applications_application_course_application_id', '{{%application_courses}}');
        $this->dropForeignKey('FK_courses_application_course_course_id', '{{%application_courses}}');
        $this->dropTable('{{%application_courses}}');
    }
}
