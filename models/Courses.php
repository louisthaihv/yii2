<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $code
 * @property float $price
 *
 * @property ApplicationCourse[] $applicationCourses
 * @property UniversityDegreeCourse[] $universityDegreeCourses
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'number'],
            [['title', 'description', 'code'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'code' => 'Code',
            'price' => 'Price',
        ];
    }

    /**
     * Gets query for [[ApplicationCourses]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\ApplicationCourseQuery
     */
    public function getApplicationCourses()
    {
        return $this->hasMany(ApplicationCourse::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[UniversityDegreeCourses]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\UniversityDegreeCourseQuery
     */
    public function getUniversityDegreeCourses()
    {
        return $this->hasMany(UniversityDegreeCourse::className(), ['course_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CoursesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CoursesQuery(get_called_class());
    }

    public function getApplications() {
        return $this->hasMany(Applications::class, ['id' => 'application_id'])
            ->viaTable('application_courses', ['course_id' => 'id']);
    }

}
