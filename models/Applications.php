<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $title
 * @property string|null $description
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ApplicationCourse[] $applicationCourses
 * @property ApplicationUniversityDegree[] $applicationUniversityDegrees
 * @property User $user
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[ApplicationCourses]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\ApplicationCourseQuery
     */
    public function getApplicationCourses()
    {
        return $this->hasMany(ApplicationCourse::className(), ['application_id' => 'id']);
    }

    /**
     * Gets query for [[ApplicationUniversityDegrees]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\ApplicationUniversityDegreeQuery
     */
    public function getApplicationUniversityDegrees()
    {
        return $this->hasMany(ApplicationUniversityDegree::className(), ['application_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\ApplicationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ApplicationsQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class
            ],
        ];
    }
}
