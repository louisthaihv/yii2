<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "university_degrees".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $code
 * @property float $price
 *
 * @property ApplicationUniversityDegree[] $applicationUniversityDegrees
 * @property UniversityDegreeCourse[] $universityDegreeCourses
 */
class UniversityDegrees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'university_degrees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'number'],
            [['title', 'description', 'code'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'code' => 'Code',
            'price' => 'Price',
        ];
    }

    /**
     * Gets query for [[ApplicationUniversityDegrees]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\ApplicationUniversityDegreeQuery
     */
    public function getApplicationUniversityDegrees()
    {
        return $this->hasMany(ApplicationUniversityDegree::className(), ['university_degree_id' => 'id']);
    }

    /**
     * Gets query for [[UniversityDegreeCourses]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\UniversityDegreeCourseQuery
     */
    public function getUniversityDegreeCourses()
    {
        return $this->hasMany(UniversityDegreeCourse::className(), ['university_degree_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\UniversityDegreesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\UniversityDegreesQuery(get_called_class());
    }

    public function getApplications() {
        return $this->hasMany(Applications::class, ['id' => 'application_id'])
            ->viaTable('application_university_degrees', ['university_degree_id' => 'id']);
    }
}
