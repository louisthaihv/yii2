<?php


namespace app\models\resources;

use app\models\Courses;

class Course extends Courses
{
    public function extraFields()
    {
        return ['applications'];
    }
}