<?php
namespace app\models\resources;

use app\models\Applications;

class Application extends Applications
{
    public function extraFields()
    {
        return ['user'];
    }
}