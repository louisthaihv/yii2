<?php
namespace app\models\resources;

use app\models\Users;
use yii\db\Exception;

class User extends Users
{
    public $applications = [];

    public function fields()
    {
        return ['id', 'username', 'first_name', 'last_name', 'email', 'phone', 'dob'];
    }

    public function extraFields()
    {
        return ['applications'];
    }

    public function load($data, $formName = null)
    {
        $this->applications = [];
        if ($this->isNewRecord && isset($data['applications'])) {

            foreach ($data['applications'] as $applicationData) {
                $application = new Application();
                $application->load($applicationData, "");
                $this->applications[] = $application;
            }
        }
        return parent::load($data, $formName);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $wasNewRecord = $this->isNewRecord;
        $result = parent::save($runValidation, $attributeNames);
        if ($wasNewRecord && $this->applications) {
            foreach ($this->applications as $application) {
                $application->user_id = $this->id;
                if (!$application->save()) {
                    $this->addError('applications', array_values($application->errors));
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        try {
            $transaction->commit();
        } catch (Exception $e) {
        }
        return $result;
    }
}
