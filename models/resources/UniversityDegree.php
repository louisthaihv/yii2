<?php


namespace app\models\resources;

use app\models\UniversityDegrees;

class UniversityDegree extends UniversityDegrees
{
    public function extraFields()
    {
        return ['applications'];
    }
}