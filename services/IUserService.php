<?php


namespace app\services;


interface IUserService
{
    public function createUser(array $payload);
}