<?php
namespace app\services;

use yii;
use app\models\resources\User;

class UserService implements IUserService
{
    private $model;
    private $response;

    public function __construct(User $user)
    {
        $this->model = $user;
        $this->response = Yii::$app->response;
    }

    public function createUser(array $payload)
    {
        $payload['User'] = $payload;
        if($this->model->load($payload)) {
            $hash = Yii::$app->getSecurity()->generatePasswordHash('123');
            $this->model->password_hash = $hash;
            // TODO why behaviors on model not working
            if ($this->model->save()) {
                $this->response->statusCode = 200;
                $this->response->data = [
                    'message' => 'create successfully!',
                    'data' => $this->model
                ];
                return $this->response;
            }
        }
        $this->response->statusCode = 422;
        $this->response->data = [
            'message' => "something went wrong!",
            'data' => $this->model->getFirstErrors()
        ];
        return $this->response;
    }
}