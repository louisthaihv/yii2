<?php


namespace app\services;

use Yii;
use sizeg\jwt\Jwt;
use sizeg\jwt\JwtHttpBearerAuth;
use app\models\resources\User;

class AuthService implements IAuthService
{

    /**
     * @param array $payload
     * @return mixed
     */
    public function getToken(array $payload)
    {
        try {
            $user = User::find()->where([
                'username' => $payload['username'],
                'status' => User::STATUS_ACTIVE
            ])->One();
            if (!$user) {
                return '';
            }
            if (!Yii::$app->getSecurity()->validatePassword($payload['password'], $user->password_hash)) {
                var_dump('sai pass');
                die;
                return '';
            }

            $jwt = Yii::$app->jwt;
            $signer = $jwt->getSigner('HS256');
            $key = $jwt->getKey();
            $time = time();

            $token = $jwt->getBuilder()
                ->issuedBy(Yii::$app->params['baseURL'])
                ->permittedFor(Yii::$app->params['baseURL'])
                ->identifiedBy(Yii::$app->params['baseURL'], true)
                ->issuedAt($time)
                ->expiresAt($time + 3600)
                ->withClaim('uid', $user->id)
                ->getToken($signer, $key);
            return (string)$token;
        } catch (\Exception $e) {
            var_dump($e);
            die;
            return '';
        }
    }

    /**
     * @param array $payload
     * @return mixed
     */
    public function findByToken(string $token)
    {
        // TODO: Implement findByToken() method.
    }
}