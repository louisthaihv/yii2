<?php


namespace app\services;


interface IAuthService
{
    public function findByToken(string $token);
}